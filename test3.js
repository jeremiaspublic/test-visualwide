let optionElement = document.getElementById("test");

function changeColor(e) {
  let color = e.target.value.toLowerCase();
  optionElement.style.color = color;
}

optionElement.addEventListener("change", changeColor);
