<?php
$array = array(1, 2, 3);
$suma_valores = 0;
function arrayMultiplier($arg_array, $arg_multiplier)
{
  try {
    global $suma_valores;
    if (!is_int($arg_multiplier)) {
      throw new Exception('Multiplier value type error');
    }
    else {
      foreach ($arg_array as &$valor) {
        if (!is_int($valor)) {
          throw new Exception('Array value type error');
        }
        else {
          $valor = $valor * $arg_multiplier;
          $suma_valores += $valor;
        }
      }
      echo $suma_valores;
      return $suma_valores;
    }
  }
  catch (Exception $e) {
    echo 'Error message: ' . $e->getMessage();

  }
}
arrayMultiplier($array, 4);
?>